roller

a simple and quick solution for tracking multiple turn orders across multiple participants.
designed for multiplayer gaming.
implemented as a live virtual counter that all participants can update in real time.

requirements:
* nodejs
* mongodb

notes:
* copy config-template.js to config.js and fill out values
* roller.service systemd service file included
* create htdocs/img/background.jpg for a site background image
