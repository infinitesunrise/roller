class Server {
  constructor() {
    return (async () => {
      const config = require("./config");

      //mongo
      const { MongoClient } = require("mongodb");
      const mongoClient = new MongoClient("mongodb://" + config.mongoHost + ":" + config.mongoPort);
      await mongoClient.connect();
      const mongo = mongoClient.db("roller");

      //http
      const querystring = require("querystring");
      const httpsServer = require("https");
      const fs = require("fs");
      const path = require('path');
      const https = httpsServer.createServer(
      {
        key: fs.readFileSync(config.privPath),
        cert: fs.readFileSync(config.certPath),
        ca: [ fs.readFileSync(config.caPath), fs.readFileSync(config.fullcaPath) ]
      },
      async function(req,res){
	      if (req.url == "/new" || req.url == "/join"){
          await handlePosts(req,res); 
        } else {
          await handleGets(req,res);
        }
      });
      https.listen(config.httpsPort);

      //websockets
      const io = require('socket.io')(https);
      io.on("connection", socket => {
        socket.on("join", async (data) => { await joinRequest(socket, data) });
        socket.on("read", async (data) => { await readRequest(socket, data) });
        socket.on("update", async (data) => { await updateRequest(socket, data) });
      });

      async function handlePosts(req, res){
        var path = req.url;
        const buffers = [];
        for await (const chunk of req) {
          buffers.push(chunk);
        }
        var dataString = querystring.unescape(Buffer.concat(buffers).toString()).replace(/\[\]/g,"");
        var data = querystring.parse(dataString);
        if (path == "/new"){
          var newRoller = {
            name: data.rollerName,
            players: data.players,
            categories: data.categories
          }
          if (Array.isArray(newRoller.players) == false){
            newRoller.players = [ newRoller.players ];
          }
          if (Array.isArray(newRoller.categories) == false){
            newRoller.categories = [ newRoller.categories ];
          }
          for (var i = 0; i < newRoller.categories.length; i++){
            newRoller["category" + i] = 0;
          }
          if (newRoller.name.length < 1){
            httpResponse(res, { error: true, errorMessage: "roller name is blank" });
            return;
          }
          if (newRoller.players.length < 2){
            httpResponse(res, { error: true, errorMessage: "at least two players are required" });
            return;
          }
          if (newRoller.categories.length < 1){
            httpResponse(res, { error: true, errorMessage: "at least one category is required" });
            return;
          }
          var exists = await mongo.collection("rollers").findOne({ name: newRoller.name });
          if (exists){
            httpResponse(res, { error: true, errorMessage: "roller name is taken" });
            return;
          }
          var insertResult = await mongo.collection("rollers").insertOne(newRoller);
          var result = {
            error: false,
            roller: insertResult.insertId,
            name: newRoller.name
          }
          httpResponse(res, result);
        }
        if (path = "/join"){
          var name = data.rollerName;
          var exists = await mongo.collection("rollers").findOne({ name: name });
          if (!exists){
            httpResponse(res, { error: true, errorMessage: "roller does not exist" });
            return;
          }
          var result = { error: false }
          httpResponse(res, result);
        }
      }

      async function handleGets(req, res){
          var filePath = "./htdocs" + req.url;
          if (filePath == "./htdocs/"){ filePath = "./htdocs/index.html"; }
          var extName = path.extname(filePath);
          var contentType = ""
          switch (extName) {
            case '.js':
              contentType = 'text/javascript';
              break;
            case '.jpg':
              contentType = 'image/jpg';
              break;
            case '.html':
              contentType = 'text/html';
              break;
          }
          if (contentType == ""){ return; }
          var data = fs.readFileSync(filePath, function(error, content) {
            if (error){ return false; }
          });
          if (data == false){ return; }
          res.writeHead(200, {'Content-Type': contentType});
          res.end(data, 'utf-8');
      }

      function httpResponse(res, data){
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(data));
      }
      
      async function joinRequest(socket, data){
        var rollerName = data.rollerName;
        socket.join(rollerName);
        await readRequest(socket, data);
      }

      async function readRequest(socket, data){
        var rollerName = data.rollerName;
        var roller = await mongo.collection("rollers").findOne({ name: rollerName });
        delete roller._id;
        io.to(socket.id).emit('roller', roller);
      }

      async function updateRequest(socket, data){
        var rollerName = data.rollerName;
        var categoryName = "category" + data.category;
        var roller = await mongo.collection("rollers").findOne({ name: rollerName });
        if (!roller){ return }
        var categoryCurrentValue = roller[categoryName]; 
        var categoryNewValue = categoryCurrentValue + 1;
        if (categoryNewValue >= roller.players.length){ categoryNewValue = 0 }
        var result = await mongo.collection("rollers").findOneAndUpdate(
          { name: rollerName },
          { $set: { [categoryName]: categoryNewValue } },
          { returnDocument: 'after' }
        );
        io.to(rollerName).emit('roller', result.value);
      }

      return this;
    })();
  }
}

ready = (async function(){ server = await new Server(); })();

