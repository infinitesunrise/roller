module.exports = {
  mongoHost: "127.0.0.1",
  mongoPort: "27017",
  httpsPort: "8080",
  certPath: "/var/certs/roller.bluntsusan.com/cert.pem",
  privPath: "/var/certs/roller.bluntsusan.com/privkey.pem",
  caPath: "/var/certs/roller.bluntsusan.com/chain.pem",
  fullcaPath: "/var/certs/roller.bluntsusan.com/fullchain.pem"
}
